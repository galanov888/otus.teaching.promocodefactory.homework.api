﻿using GraphQL;
using GraphQL.Types;
using GraphQLCustomers.GraphQL.GraphQLTypes;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLCustomers.GraphQL.GraphQLQueries
{
    public class AppMutation : ObjectGraphType
    {
        public AppMutation(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            Field<IdGraphType>(
                "createCustomer",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<CustomerInputType>> { Name = "customer" },
                 new QueryArgument<NonNullGraphType<ListGraphType<IdGraphType>>> { Name = "preferenceIds" }),
                resolve: context =>
                {
                    var customer = context.GetArgument<Customer>("customer");
                    var preferenceIds = context.GetArgument<List<Guid>>("preferenceIds");
                    var dbPreferences = preferenceRepository.GetRangeByIdsAsync(preferenceIds).Result;
                    var customerId = Guid.NewGuid();
                    customer.Preferences = dbPreferences.Select(x => new CustomerPreference()
                    {
                        CustomerId = customerId,
                        Preference = x,
                        PreferenceId = x.Id
                    }).ToList();
                    customerRepository.AddAsync(customer);
                    return customerId;
                });

            Field<IdGraphType>(
                "updateCustomer",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<CustomerInputType>> { Name = "customer" },
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "customerId" },
                    new QueryArgument<NonNullGraphType<ListGraphType<IdGraphType>>> { Name = "preferenceIds"}),
                resolve: context =>
                {
                    var customer = context.GetArgument<Customer>("customer");
                    var customerId = context.GetArgument<Guid>("customerId");
                    var dbCustomer = customerRepository.GetByIdAsync(customerId).Result;
                    if (dbCustomer == null)
                    {
                        context.Errors.Add(new ExecutionError("Couldn't find customer in db."));
                        return null;
                    }
                    var preferenceIds = context.GetArgument<List<Guid>>("preferenceIds");
                    var dbPreferences = preferenceRepository.GetRangeByIdsAsync(preferenceIds).Result;
                    dbCustomer.Preferences = dbPreferences.Select(x => new CustomerPreference()
                    {
                        CustomerId = customer.Id,
                        Preference = x,
                        PreferenceId = x.Id
                    }).ToList();
                    dbCustomer.Email = customer.Email;
                    dbCustomer.FirstName = customer.FirstName;
                    dbCustomer.LastName = customer.LastName;
                    customerRepository.UpdateAsync(dbCustomer);
                    return customerId;
                });

            Field<StringGraphType>(
                "deleteCustomer",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "customerId" }),
                resolve: context =>
                {
                    var customerId = context.GetArgument<Guid>("customerId");
                    var customer = customerRepository.GetByIdAsync(customerId).Result;
                    if (customer == null)
                    {
                        context.Errors.Add(new ExecutionError("Couldn't find customer in db."));
                        return null;
                    }
                    customerRepository.DeleteAsync(customer);
                    return $"The customer with the id: {customerId} has been successfully deleted from db.";
                });
        }
    }
}
