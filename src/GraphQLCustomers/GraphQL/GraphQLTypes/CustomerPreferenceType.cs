﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace GraphQLCustomers.GraphQL.GraphQLTypes
{
    public class CustomerPreferenceType : ObjectGraphType<CustomerPreference>
    {
        public CustomerPreferenceType()
        {
            Field(x => x.CustomerId, type: typeof(IdGraphType)).Description("CustomerId property from the CustomerPreference object.");
            Field(x => x.PreferenceId, type: typeof(IdGraphType)).Description("PreferenceId property from the CustomerPreference object.");
            Field<CustomerType>("customer", resolve: context => {
                return context.Source.Customer;
                 });
            Field<PreferenceType>("preference", resolve: context => {
                return context.Source.Preference;
            });
        }
    }
}
