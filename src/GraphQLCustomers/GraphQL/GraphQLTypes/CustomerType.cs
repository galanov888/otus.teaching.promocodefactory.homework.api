﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.DataLoader;
using GraphQL.Types;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace GraphQLCustomers.GraphQL.GraphQLTypes
{
    public class CustomerType : ObjectGraphType<Customer>
    {
        public CustomerType()//IDataLoaderContextAccessor dataLoader, IRepository)
        {
            Field(x => x.Id, type: typeof(IdGraphType)).Description("Id property from the Customer object.");
            Field(x => x.FirstName).Description("FirstName property from the Customer object.");
            Field(x => x.LastName).Description("LastName property from the Customer object.");
            Field(x => x.Email).Description("Email property from the Customer object.");

            Field<ListGraphType<CustomerPreferenceType>>(
                "preferences",
                resolve: context =>
                {
                    return context.Source.Preferences;
                }
            );
        }
    }
}
