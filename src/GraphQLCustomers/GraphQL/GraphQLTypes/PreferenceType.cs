﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace GraphQLCustomers.GraphQL.GraphQLTypes
{
    public class PreferenceType : ObjectGraphType<Preference>
    {
        public PreferenceType()
        {
            Field(x => x.Id, type: typeof(IdGraphType)).Description("Id property from the Preference object.");
            Field(x => x.Name).Description("Name property from the Preference object.");
        }
    }
}
