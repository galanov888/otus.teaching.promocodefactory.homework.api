using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using PromoCodeManagement = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GrpcGreeter
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        private readonly IRepository<PromoCodeManagement.Customer> _customerRepository;
        private readonly IRepository<PromoCodeManagement.Preference> _preferenceRepository;

        public GreeterService(ILogger<GreeterService> logger, IRepository<PromoCodeManagement.Customer> customerRepository, IRepository<PromoCodeManagement.Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<GetAllReply> GetAll(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = new GetAllReply();
            response.Customers.AddRange(customers.Select(c => new Customer
            {
                Id = c.Id.ToString(),
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }));
            return response;
        }

        public override async Task<CreateCustomerResponse> CreateCustomerAsync(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferencesIds = request.PreferenceIds.Select(p => Guid.Parse(p)).ToList();
            var preferences = await _preferenceRepository
               .GetRangeByIdsAsync(preferencesIds);

            var customer = MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);
            return new CreateCustomerResponse
            {
                CustomerId = customer.Id.ToString()
            };
        }

        private static PromoCodeManagement.Customer MapFromModel(CreateCustomerRequest model, IEnumerable<PromoCodeManagement.Preference> preferences)
        {
            var customer = new PromoCodeManagement.Customer();
            customer.Id = Guid.NewGuid();

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new PromoCodeManagement.CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public override async Task<Empty> EditCustomerAsync(EditCustomerRequest request, ServerCallContext context)
        {
            var customerId = Guid.Parse(request.CustomerId);
            var customer = await _customerRepository.GetByIdAsync(customerId);

            if (customer == null)
                 new ArgumentNullException();

            var preferencesIds = request.PreferenceIds.Select(p => Guid.Parse(p)).ToList();
            var preferences = await _preferenceRepository
               .GetRangeByIdsAsync(preferencesIds);

            MapFromModel(request, preferences,customer);
            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        private static PromoCodeManagement.Customer MapFromModel(EditCustomerRequest model, IEnumerable<PromoCodeManagement.Preference> preferences, PromoCodeManagement.Customer customer = null)
        {
            if (customer == null)
            {
                customer = new PromoCodeManagement.Customer();
                customer.Id = Guid.NewGuid();
            }

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new PromoCodeManagement.CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public override async Task<Empty> DeleteCustomerAsync(DeleteCustomerAsyncRequest request, ServerCallContext context)
        {
            var customerId = Guid.Parse(request.CustomerId);
            var customer = await _customerRepository.GetByIdAsync(customerId);
            if (customer == null)
            {
                new ArgumentNullException();
            }

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }

        public override async Task<GetCustomerAsyncResponse> GetCustomerAsync(GetCustomerAsyncRequest request, ServerCallContext context)
        {
            var customerId = Guid.Parse(request.CustomerId);
            var customer = await _customerRepository.GetByIdAsync(customerId);
            var response = new GetCustomerAsyncResponse();
            response.Customer = new Customer
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
                
            };
            response.Preferences.AddRange(customer.Preferences.Select(p => new Preference
            {
                Id = p.Preference.Id.ToString(),
                Name = p.Preference.Name
            }));

            return response;
        }
    }
}
