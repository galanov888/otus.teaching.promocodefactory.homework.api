using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Grpc.Net.Client;
using GrpcGreeter = Otus.Teaching.PromoCodeFactory.GrpcGreeter;
using Google.Protobuf.Collections;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersGRPCController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersGRPCController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        private GrpcGreeter.Greeter.GreeterClient GetGrpcClient()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5005");
            var client =  new GrpcGreeter.Greeter.GreeterClient(channel);
            return client;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5005");
            var client = new GrpcGreeter.Greeter.GreeterClient(channel);
            var grpcResponse = await client.GetAllAsync(new GrpcGreeter.Empty());
            var response = new List<CustomerShortResponse>();
            response.AddRange(grpcResponse.Customers.Select(c => new CustomerShortResponse
            {
                Id = Guid.Parse(c.Id),
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }));
            return Ok(response);
        }
        
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var client = GetGrpcClient();
            var customerId =  id.ToString();
            var request = new GrpcGreeter.GetCustomerAsyncRequest { CustomerId = customerId };
            var customerGrpc = await client.GetCustomerAsyncAsync(request);

            var response = new CustomerResponse
            {
                Id = Guid.Parse(customerGrpc.Customer.Id),
                Email = customerGrpc.Customer.Email,
                FirstName = customerGrpc.Customer.FirstName,
                LastName = customerGrpc.Customer.LastName

            };
            response.Preferences = customerGrpc.Preferences.Select(p => new PreferenceResponse
            {
                Id = Guid.Parse(p.Id),
                Name = p.Name
            }).ToList();

            response.PromoCodes = customerGrpc.PromoCodes.Select(pc => new PromoCodeShortResponse
            {
                BeginDate = pc.BeginDate,
                Code = pc.Code,
                EndDate = pc.EndDate,
                Id = Guid.Parse(pc.Id),
                PartnerName = pc.PartnerName,
                ServiceInfo = pc.ServiceInfo
            }).ToList();

            return Ok(response);
        }
        
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект

            var client = GetGrpcClient();
            var grpcRequest = new GrpcGreeter.CreateCustomerRequest
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
            };
            grpcRequest.PreferenceIds.AddRange(request.PreferenceIds.Select(p => p.ToString()));
            
            var customerResponse = await client.CreateCustomerAsyncAsync(grpcRequest);
            var customerId = Guid.Parse(customerResponse.CustomerId);
            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customerId }, customerId);
        }
        
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var client = GetGrpcClient();
            var grpcRequest = new GrpcGreeter.EditCustomerRequest
            {
                CustomerId = id.ToString(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };
            grpcRequest.PreferenceIds.AddRange(request.PreferenceIds.Select(p => p.ToString()));

            await client.EditCustomerAsyncAsync(grpcRequest);

            return NoContent();
        }
        
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var client = GetGrpcClient();
            var grpcRequest = new GrpcGreeter.DeleteCustomerAsyncRequest
            {
                CustomerId = id.ToString()
            };

            await client.DeleteCustomerAsyncAsync(grpcRequest);

            return NoContent();
        }
    }
}