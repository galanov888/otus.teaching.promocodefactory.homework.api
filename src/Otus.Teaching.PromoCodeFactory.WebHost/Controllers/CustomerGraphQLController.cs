﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.GraphQL;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerGraphQLController : ControllerBase
    {
        private readonly CustomerConsumer _consumer;

        public CustomerGraphQLController(CustomerConsumer consumer)
        {
            _consumer = consumer;
        }

        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _consumer.GetAllCustomersAsync();
            var response = new List<CustomerShortResponse>();
            response.AddRange(customers.Select(c => new CustomerShortResponse
            {
                Id = c.Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }));
            return Ok(response);
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _consumer.GetCustomerByIdAsync(id);
            var response = new CustomerResponse(customer);

            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var customerInput = new CustomerInput()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };

            var customerId = await _consumer.CreateCustomerAsync(customerInput, request.PreferenceIds);

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customerId }, customerId);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _consumer.GetCustomerByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerInput = new CustomerInput()
            {
                
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };

            await _consumer.UpdateCustomer(id, customerInput, request.PreferenceIds);

            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            
            await _consumer.DeleteCustomer(id);

            return NoContent();
        }
    }
}
