﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Client.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Response;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class CustomerConsumer
    {
        private readonly IGraphQLClient _client;

        public CustomerConsumer(IGraphQLClient client)
        {
            _client = client;
        }

        public async Task<List<Customer>> GetAllCustomersAsync()
        {
            var query = new GraphQLRequest
            {
                Query = @"
                    query customersQuery{
                        customers{
                            id,
                            firstName,
                            lastName,
                            email
                        }
                    }"
            };
            var response = await _client.SendQueryAsync<ResponseCustomerCollectionType>(query);
            if ((response.Errors?.Count() ?? 0) != 0)
            {
                throw new Exception(response.Errors.First().Message);
            }
            return response.Data.Customers;
        }

        public async Task<Customer> GetCustomerByIdAsync(Guid id)
        {
            var query = new GraphQLRequest
            {
                Query = @"
                   query customerQuery($customerId: ID!){
                      customer(customerId: $customerId)
                      {
                        id,
                        lastName,
                        firstName,
                        email,
                        preferences{
                          preferenceId
                          preference{
                            id,
                            name
                          }
                        }
                      }
                    }",
                Variables = new { customerId = id }
            };
            var response = await _client.SendQueryAsync<ResponseCustomerType>(query);
            if ((response.Errors?.Count() ?? 0) != 0)
            {
                throw new Exception(response.Errors.First().Message);
            }
            return response.Data.Customer;
        }

        public async Task<Guid> CreateCustomerAsync(CustomerInput customerInput, List<Guid> preferenceIds)
        {
            var query = new GraphQLRequest
            {
                Query = @"
mutation($customer: customerInput!, $preferenceIds: [ID!]!){
  createCustomer(customer: $customer, preferenceIds: $preferenceIds)
}
",
                Variables = new { customer = customerInput, preferenceIds = preferenceIds }
            };
            var response = await _client.SendMutationAsync<ResponseCreateCustomerIdType>(query);
            if ((response.Errors?.Count() ?? 0) != 0)
            {
                throw new Exception(response.Errors.First().Message);
            }
            
            return response.Data.CreateCustomer;
        }

        public async Task UpdateCustomer(Guid id, CustomerInput customerToUpdate, List<Guid> preferenceIds)
        {
            var query = new GraphQLRequest
            {
                Query = @"
mutation($customer: customerInput!, $customerId: ID!, $preferenceIds: [ID!]!){
  updateCustomer(customer: $customer, preferenceIds: $preferenceIds, customerId: $customerId)
}
",
                Variables = new { customer = customerToUpdate, preferenceIds = preferenceIds, customerId = id,  }
            };
            var response = await _client.SendMutationAsync<ResponseUpdateCustomerIdType>(query);
            if ((response.Errors?.Count() ?? 0) != 0)
            {
                throw new Exception(response.Errors.First().Message);
            }
        }

        public async Task<string> DeleteCustomer (Guid id)
        {
            var query = new GraphQLRequest
            {
                Query = @"
               mutation($customerId: ID!){
                  deleteCustomer(customerId: $customerId)
                }",
                Variables = new { customerId = id }
            };

            var response = await _client.SendMutationAsync<ResponseDeleteCustomerType>(query);
            if ((response.Errors?.Count() ?? 0) != 0)
            {
                throw new Exception(response.Errors.First().Message);
            }
            return response.Data.DeleteCustomer;
        }
    }
}
