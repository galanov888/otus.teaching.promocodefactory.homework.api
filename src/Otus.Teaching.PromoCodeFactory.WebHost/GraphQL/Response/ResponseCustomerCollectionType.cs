﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Response
{
    public class ResponseCustomerCollectionType
    {
        public List<Customer> Customers { get; set; }
    }

    public class ResponseCustomerType
    {
        public Customer Customer { get; set; }
    }

    public class ResponseCreateCustomerIdType
    {
        public Guid CreateCustomer { get; set; }
    }

    public class ResponseUpdateCustomerIdType
    {
        public Guid UpdateCustomer { get; set; }
    }

    public class ResponseDeleteCustomerType
    {
        public string DeleteCustomer { get; set; }
    }
}
