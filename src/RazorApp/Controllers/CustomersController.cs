﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using RazorApp.Models;

namespace RazorApp.Controllers
{
    public class CustomersController : Controller
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        // GET: Customers
        public async Task<IActionResult> Index()
        {
            return View(await _customerRepository.GetAllAsync());
        }

        // GET: Customers/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _customerRepository.GetByIdAsync(id.Value);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // GET: Customers/Create
        public async Task<IActionResult> Create()
        {
            ViewBag.Preferences = await _preferenceRepository.GetAllAsync();
            //ViewBag.MyPreferences = await _preferenceRepository.GetWhere(p=>p.Id == Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"));
           
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateOrEditCustomerRequest customerRequest)//([Bind("FirstName,LastName,Email,Id")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                customerRequest.Customer.Id = Guid.NewGuid();
                var preferences = await _preferenceRepository.GetRangeByIdsAsync(customerRequest.PreferenceIds.ToList());
                var customerPreference = preferences.Select(p => new CustomerPreference
                {
                    CustomerId = customerRequest.Customer.Id,
                    PreferenceId = p.Id
                });
                customerRequest.Customer.Preferences = customerPreference.ToList();
                await _customerRepository.AddAsync(customerRequest.Customer);
                return RedirectToAction(nameof(Index));
            }
            return View(customerRequest.Customer);
        }

        // GET: Customers/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _customerRepository.GetByIdAsync(id.Value);
            if (customer == null)
            {
                return NotFound();
            }
            var response = new CreateOrEditCustomerRequest
            {
                Customer = customer,
                PreferenceIds = customer.Preferences.Select(p => p.PreferenceId)
            };
            //ViewBag.MyPreferences = new List<Guid> { Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84") };
            ViewBag.Preferences = await _preferenceRepository.GetAllAsync();
            return View(response);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, CreateOrEditCustomerRequest customerRequest)//[Bind("FirstName,LastName,Email,Id")] Customer customer)
        {
            if (id != customerRequest.Customer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var dbCustomer = await _customerRepository.GetByIdAsync(customerRequest.Customer.Id);
                    var preferences = await _preferenceRepository.GetRangeByIdsAsync(customerRequest.PreferenceIds.ToList());
                    var customerPreference = preferences.Select(p => new CustomerPreference
                    {
                        CustomerId = customerRequest.Customer.Id,
                        PreferenceId = p.Id,
                        Preference = p
                    }).ToList();
                    dbCustomer.FirstName = customerRequest.Customer.FirstName;
                    dbCustomer.LastName = customerRequest.Customer.LastName;
                    dbCustomer.Email = customerRequest.Customer.Email;
                    dbCustomer.Preferences = customerPreference;
                    await _customerRepository.UpdateAsync(dbCustomer);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(customerRequest.Customer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(customerRequest.Customer);
        }

        // GET: Customers/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _customerRepository.GetByIdAsync(id.Value);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            await _customerRepository.DeleteAsync(customer);
            return RedirectToAction(nameof(Index));
        }

        private bool CustomerExists(Guid id)
        {
            var result =  _customerRepository.GetWhere(e => e.Id == id).Result;
            return result.Any();
        }
    }
}
