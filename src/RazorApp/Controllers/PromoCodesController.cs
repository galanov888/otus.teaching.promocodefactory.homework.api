﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace RazorApp.Controllers
{
    public class PromoCodesController : Controller
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Employee> _employeesRepository;
        private readonly IRepository<Preference> _preferencesRepository;

        public PromoCodesController(IRepository<PromoCode> promoCodesRepository,
            IRepository<Employee> employeesRepository,
            IRepository<Preference> preferencesRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _employeesRepository = employeesRepository;
            _preferencesRepository = preferencesRepository;
        }

        // GET: PromoCodes
        public async Task<IActionResult> Index()
        {
            var promoCodes =await  _promoCodesRepository.GetAllAsync();
            return View(promoCodes);
        }

        // GET: PromoCodes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);
            if (promoCode == null)
            {
                return NotFound();
            }

            return View(promoCode);
        }

        // GET: PromoCodes/Create
        public async Task<IActionResult> Create()
        {
            ViewData["PartnerManagerId"] = new SelectList( await _employeesRepository.GetAllAsync(), "Id", "FullName");
            ViewData["PreferenceId"] = new SelectList( await _preferencesRepository.GetAllAsync(),  "Id", "Name");
            return View();
        }

        // POST: PromoCodes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Code,ServiceInfo,BeginDate,EndDate,PartnerName,PartnerManagerId,PreferenceId,Id")] PromoCode promoCode)
        {
            if (ModelState.IsValid)
            {
                promoCode.Id = Guid.NewGuid();
                await _promoCodesRepository.AddAsync(promoCode);
                return RedirectToAction(nameof(Index));
            }
            ViewData["PartnerManagerId"] = new SelectList(await _employeesRepository.GetAllAsync(), "Id", "FullName");
            ViewData["PreferenceId"] = new SelectList(await _preferencesRepository.GetAllAsync(), "Id", "Name");
            return View(promoCode);
        }

        // GET: PromoCodes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);
            if (promoCode == null)
            {
                return NotFound();
            }
            ViewData["PartnerManagerId"] = new SelectList(await _employeesRepository.GetAllAsync(), "Id", "FullName");
            ViewData["PreferenceId"] = new SelectList(await _preferencesRepository.GetAllAsync(), "Id", "Name");
            return View(promoCode);
        }

        // POST: PromoCodes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Code,ServiceInfo,BeginDate,EndDate,PartnerName,PartnerManagerId,PreferenceId,Id")] PromoCode promoCode)
        {
            if (id != promoCode.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _promoCodesRepository.UpdateAsync(promoCode);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PromoCodeExists(promoCode.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PartnerManagerId"] = new SelectList(await _employeesRepository.GetAllAsync(), "Id", "FullName");
            ViewData["PreferenceId"] = new SelectList(await _preferencesRepository.GetAllAsync(), "Id", "Name"); 
            return View(promoCode);
        }

        // GET: PromoCodes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);
            if (promoCode == null)
            {
                return NotFound();
            }

            return View(promoCode);
        }

        // POST: PromoCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);
            await _promoCodesRepository.DeleteAsync(promoCode);
            return RedirectToAction(nameof(Index));
        }

        private bool PromoCodeExists(Guid id)
        {
            var result = _promoCodesRepository.GetWhere(e => e.Id == id).Result;
            return result.Any();
        }
    }
}
