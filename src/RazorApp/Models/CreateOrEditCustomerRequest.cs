﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RazorApp.Models
{
    public class CreateOrEditCustomerRequest
    {
        public Customer Customer { get; set; }
        public IEnumerable<Guid> PreferenceIds { get; set; }
    }
}
